console.log("Judy");


// SECTIoN [JSoN] - JS Object Notation 
  // Syntax: { "properyA": "valueA"}


// {

// 	"city": "taguig",
// 	"province": "laguna",
// 	"contry": "PHL",

// }




// [Section] JSON Arrays


// "cities": [

// 	{"city": "quezon", "province": "Laguna", "country":	"Philippines"},

// ]

// "bands": [

// {
// 	"name": "All American Rejects", 
// 	"song": "Move Along", 
// 	"gender": "Male"
// },

// ]


// [Section] JSON Methods
	// converting data into stringfied JSON


// - Stringified JSON is a JavaScript object converted into a string to be used in other functions of a JavaScript application
//	- They are commonly used in HTTP requests where information is required to be sent and received in a stringified JSON format
//	- Requests are an important part of programming where an application communicates with a backend application to perform different tasks such as getting/creating data in a database


let batches = [{
	batchName: "Batch244",
	description: "Part-time"
}, 
{
	batchName: "Batch 243",
	description: "Full time"
}
];


// "stringify" method used to convert JS objects into a string 
console.log(batches);
console.log(JSON.stringify(batches));


let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "PHL"
	}
});

console.log(data);

// using stringify  method with variables

// let firstName = prompt("Please enter your username:");
// let lastName = prompt("Please enter your password");
// let age = prompt("How old are you?");
// let address = {
// 	city: prompt("Enter your city"),
// 	country: prompt("Enter your country")
// };


// let otherData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// });

// console.log(otherData);





// Section [JSoN converting stringfied JSON into JS objects]

let batchesJSON = `[{"batchName": "Batch 244"}, {"batchName": "Batch 243"}, {"id": "1", "Name": "judy"}]`
console.log(batchesJSON);

console.log('result from parse method: ');
console.log(JSON.parse(batchesJSON));


let stringifiedObject = `[{
	"id": "1",
	"name": "Judy",
	"age" : "30",
	"address": {
		"city": "Bay",
		"country": "PHL"
	},

	"id": "2",
	"name": "Maria",
	"age" : "35",
	"address": {
		"city": "LB",
		"country": "PHL" 
	}


}]`

console.log(JSON.parse(stringifiedObject));


let users = `[

{"id": "1", "name": "Leanne Graham", "username": "Bret", "email": "Sincere@april.biz", "address": {"street": "Kulas Light", "suite": "Apt. 556", "city": "gwenborough", "zipcode": "92998-3874"}, "phone": "1-770-736-8031 x56442", "website": "hildegard.org", "company": {"name": "Romaguera-Crona", "catchPhrase": "multi-layered client-server neural-net", "bs": "harness real-time -emarkets"}}, 

{"id": "2", "name": "Ervin Howell", "username": "Antonette", "email": "Shanna@melissa.tv", "address": {"street": "Victor Plains", "suite": "Suite 879", "city": "Wisokyburgh", "zipcode": "90566-7771"}, "geo": {"lat": "-43.9509", "lng": "-34.4618"}, "phone": "010-692-6593 x09125", "website": "anastasia.net", "company": {"name": "Deckow-Crist", "catchPhrase": "Proactive didactic contingency", "bs": "synergize scalable supply-chains"}},

  {
      "id": "3",
      "name": "Clementine Bauch",
      "username": "Samantha",
      "email": "Nathan@yesenia.net",
      "address": {
        "street": "Douglas Extension",
        "suite": "Suite 847",
        "city": "McKenziehaven",
        "zipcode": "59590-4157" 
      }, 
        "geo": {
          "lat": "-68.6102", 
          "lng": "-47.0653"
        }, 
      "phone": "1-463-123-4447", 
      "website": "ramiro.info", 
      "company": { 
        "name": "Romaguera-Jacobson", 
        "catchPhrase": "Face to face bifurcated interface", 
        "bs": "e-enable strategic applications"
      }},

      {
      "id": "4", 
      "name": "Patricia Lebsack", 
      "username": "Karianne", 
      "email": "Julianne.OConner@kory.org", 
      "address": {
        "street": "Hoeger Mall", 
        "suite": "Apt. 692", 
        "city": "South Elvis", 
        "zipcode": "53919-4257"
      }, 
        "geo": {
          "lat": "29.4572", 
          "lng": "-164.2990" 
        }, 

      "phone": "493-170-9623 x156", 
      "website": "kale.biz", 
      "company": {
        "name": "Robel-Corkery", 
        "catchPhrase": "Multi-tiered zero tolerance productivity", 
        "bs": "transition cutting-edge web services"
      }},

      {
      "id": "5", 
      "name": "Chelsey Dietrich", 
      "username": "Kamren", 
      "email": "Lucio_Hettinger@annie.ca", 
      "address": {
        "street": "Skiles Walks", 
        "suite": "Suite 351", 
        "city": "Roscoeview", 
        "zipcode": "33263"
      }, 
        "geo": {
          "lat": "-31.8129", 
          "lng": "62.5342"
      }, 
      "phone": "(254)954-1289", 
      "website": "demarco.info", 
      "company": {
        "name": "Keebler LLC", 
        "catchPhrase": "User-centric fault-tolerant solution", 
        "bs": "revolutionize end-to-end systems"
      }},

     {
      "id": "6", 
      "name": "Mrs. Dennis Schulist", 
      "username": "Leopoldo_Corkery", 
      "email": "Karley_Dach@jasper.info", 
      "address": { 
        "street": "Norberto Crossing", 
        "suite": "Apt. 950", 
        "city": "South Christy", 
        "zipcode": "23505-1337"
      }, 
        "geo": {
          "lat": "-71.4197", 
          "lng": "71.7478"
      }, 
      "phone": "1-477-935-8478 x6430", 
      "website": "ola.org", 
      "company": {
        "name": "Considine-Lockman", 
        "catchPhrase": "Synchronised bottom-line interface", 
        "bs": "e-enable innovative applications"
      }},

      {
      "id": "7", 
      "name": "Kurtis Weissnat", 
      "username": "Elwyn.Skiles", 
      "email": "Telly.Hoeger@billy.biz", 
      "address": {
        "street": "Rex Trail", 
        "suite": "Suite 280", 
        "city": "Howemouth", 
        "zipcode": "58804-1099"
      }, 
        "geo": { 
          "lat": "24.8918", 
          "lng": "21.8984"
      }, 
      "phone": "210.067.6132", 
      "website": "elvis.io", 
      "company": {
        "name": "Johns Group", 
        "catchPhrase": "Configurable multimedia task-force", 
        "bs": "generate enterprise e-tailers"
      }},

     {
      "id": "8", 
      "name": "Nicholas Runolfsdottir V", 
      "username": "Maxime_Nienow", 
      "email": "Sherwood@rosamond.me", 
      "address": {
        "street": "Ellsworth Summit", 
        "suite": "Suite 729", 
        "city": "Aliyaview", 
        "zipcode": "45169"
      }, 
        "geo": {
          "lat": "-14.3990", 
          "lng": "-120.7677"
        },

      "phone": "586.493.6943 x140", 
      "website": "jacynthe.com", 
      "company": {
        "name": "Abernathy Group", 
        "catchPhrase": "Implemented secondary concept", 
        "bs": "e-enable extensible e-tailers"
      }},

      {
      "id": "9", 
      "name": "Glenna Reichert", 
      "username": "Delphine", 
      "email": "Chaim_McDermott@dana.io", 
      "address": {
        "street": "Dayna Park", 
        "suite": "Suite 449", 
        "city": "Bartholomebury", 
        "zipcode": "76495-3109"
    	}, 
        "geo": {
          "lat": "24.6463", 
          "lng": "-168.8889"
        },

      "phone": "(775)976-6794 x41206", 
      "website": "conrad.com", 
      "company": {
        "name": "Yost and Sons", 
        "catchPhrase": "Switchable contextually-based project", 
        "bs": "aggregate real-time technologies"
      }},
      

      {
      "id": "10", 
      "name": "Clementina DuBuque", 
      "username": "Moriah.Stanton", 
      "email": "Rey.Padberg@karina.biz", 
      "address": {
        "street": "Kattie Turnpike", 
        "suite": "Suite 198", 
        "city": "Lebsackbury", 
        "zipcode": "31428-2261"
      }, 
        "geo": {
          "lat": "-38.2386", 
          "lng": "57.2232"
        },
      "phone": "024-648-3804", 
      "website": "ambrose.net", 
      "company": {
        "name": "Hoeger LLC", 
        "catchPhrase": "Centralized empowering task-force", 
        "bs": "target end-to-end models"
      }}

     


]`


console.log(JSON.parse(users));




// // console.log("Hello World!")

// // [SECTION] JSON 
// /*
// 	- JavaScript Object Notation - is a data format used by applications to store and transport data to one another.
// 	- Core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript objects
// 	- JavaScript objects are not to be confused with JSON
// 	- JSON is used for serializing different data types into bytes
// 	- Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
// 	- Uses double quotes for property names
// 	Syntax:
// 	{
// 		"propertyA": "valueA",
// 		"propertyB": "valueB"
// 	}
// */

// // [SECTION] JSON Objects
// 	// {
// 	// 	"city" : "Quezon City",
// 	// 	"province" : "Metro Manila",
// 	// 	"country": "Philippines"
// 	// }

// // [SECTION] JSON Arrays

// // "cities": [
// // 	{ "city": "Quezon City", "province": "Metro Manila","country": "Philippines"},
// // 	{ "city": "Manila", "province": "Metro Manila","country": "Philippines"},
// // 	{ "city": "Makati", "province": "Metro Manila","country": "Philippines"}
// // ]

// // "singers": [
// // 	{
// // 		"name": "Jason Mraz",
// // 		"song": "I'm Yours", 
// // 		"gender": "male"
// // 	},
// // 	{
// // 		"name": "Jason Derulo",
// // 		"song": "Trumpets", 
// // 		"gender": "male"
// // 	},
// // 	{
// // 		"name": "Ara Mina",
// // 		"song": "Aray", 
// // 		"gender": "female"
// // 	}
// // ]

// // [SECTION] JSON Methods

// // Converting Data into Stringified JSON
// 	/*
// 		- Stringified JSON is a JavaScript object converted into a string to be used in other functions of a JavaScript application
// 		- They are commonly used in HTTP requests where information is required to be sent and received in a stringified JSON format
// 		- Requests are an important part of programming where an application communicates with a backend application to perform different tasks such as getting/creating data in a database
// 	*/
// let batches = [
// 	{
// 		batchName: "Batch 244",
// 		description: "Part-time"
// 	},
// 	{
// 		batchName: "Batch 243",
// 		description: "Full-time"
// 	}
// ];
// console.log(batches);

// // "stringify" method is used to convert JabaScript objects into a string
// console.log(`Result from stringify method:`);
// console.log(JSON.stringify(batches));

// let data = JSON.stringify({
// 	name: "John",
// 	age: 31,
// 	address: {
// 		city: "Manila",
// 		country: "Philippines"
// 	}
// });
// console.log(data);

// // Using Stringify Method with Variables

// /*
// 	- When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable
// 	- The "property" name and "value" would have the same name which can be confusing for beginners
// 	- This is done on purpose for code readability meaning when an information is stored in a variable and when the object created to be stringified is created, we supply the variable name instead of a hard coded value
// 	- This is commonly used when the information to be stored and sent to a backend application will come from a frontend application
// 	- Syntax
// 		JSON.stringify({
// 			propertyA: variableA,
// 			propertyB: variableB
// 		});
// 	- Since we do not have a frontend application yet, we will use the prompt method in order to gather user data to be supplied to the user details
// */

// // let firstName = prompt(`What is your first name?`);
// // let lastName = prompt("What is your last name?");
// // let age = prompt("How old are you?");
// // let address = {
// // 	city: prompt("Which city do you live in?"),
// // 	country: prompt("Which country does your city belong to?")
// // };

// // let otherData = JSON.stringify({
// // 	firstName: firstName,
// // 	lastName: lastName,
// // 	age: age,
// // 	address: address
// // });
// // console.log(otherData);

// // [SECTION] Converting stringified JSON into JS Objects

// 	/*
// 		- Objects are common data types used in applications because of the complex data structures that can be created out of them
// 		- Information is commonly sent to applications in stringified JSON and then converted back into objects
// 		- This happens both for sending information to a backend application and sending information back to a frontend application
// 	*/
// let batchesJSON = `[
// 	{"batchName": "Batch 244"}, 
// 	{"batchName": "Batch 243"}
// ]`
// console.log(batchesJSON);

// console.log(`Result from parse method:`);
// console.log(JSON.parse(batchesJSON));

// let stringifiedObject = `{
// 	"name": "John",
// 	"age" : "31",
// 	"address": {
// 		"city": "Manila",
// 		"country": "Philippines"
// 	}
// }`
// console.log(JSON.parse(stringifiedObject));

// let stringifiedObject1 = JSON.parse(`{
// 	"name": "John",
// 	"age" : "31",
// 	"address": {
// 		"city": "Manila",
// 		"country": "Philippines"
// 	}
// }`);
// console.log(stringifiedObject1)




